import collections
import logging
import threading
import time

from dateutil import parser as date_parser
from http import HTTPStatus
from handler import get_boto3_from_key
from utils import SingletonGauge


def aws_sms_failed_metric():
    boto3_with_session = get_boto3_from_key(
        aws_access_key_id='AKIAR3J7VNATZ6MQ2PXO', 
        aws_secret_access_key='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',
        region_name='us-west-2',
    )
    logs_client = boto3_with_session.client('logs')

    log_group = 'sns/us-west-2/127371929639/DirectPublishToPhoneNumber/Failure'
    query = 'fields @timestamp, delivery.providerResponse'

    query_response = logs_client.start_query(
        logGroupName=log_group, 
        startTime=int(time.time() - 600),
        endTime=int(time.time()), 
        queryString=query,
    )

    if query_response['ResponseMetadata']['HTTPStatusCode'] != HTTPStatus.OK:
        print(query_response)
        return

    time.sleep(1)   # wait sometime for data is ready
    logs_result = logs_client.get_query_results(queryId=query_response['queryId'])
    
    err_reasons = [_r[1]['value'] for _r in logs_result['results']]
    counter = collections.Counter(err_reasons)

    for k, v in counter.items():
        key_snakes_name = k.lower().replace(' ', '_').replace('/', '_')
        _g = SingletonGauge(key_snakes_name, k)
        _g.inc(v)
