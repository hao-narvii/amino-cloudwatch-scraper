import boto3
import os

def get_boto3_from_key(aws_access_key_id=None, aws_secret_access_key=None, region_name=None):
    if aws_access_key_id is None:
        aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    if aws_secret_access_key is None:
        aws_secret_access_key = os.getenv('AWS_ACCESS_KEY_SECRET')
    if region_name is None:
        region_name = os.getenv('AWS_REGION_ID')

    return boto3.session.Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name,
    )

def get_boto3_from_profile(profile_name):
    return boto3.session.Session(
        profile_name=profile_name
    )
