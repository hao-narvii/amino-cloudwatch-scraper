import logging
import sys
import threading
import time

from prometheus_client import make_wsgi_app
from wsgiref.simple_server import make_server
from handler.aws_sns import (
    aws_sms_failed_metric,
)

logger = logging.getLogger('gunicorn.error')

tasks = [
    (aws_sms_failed_metric, 600),
]

threads = []

def auto_run(func, sleep_secs):
    from functools import wraps
    @wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            time_start = time.time()
            func(*args, **kwargs)
            logger.info('Run {}, time used: {}s'.format(func.__name__, time.time() - time_start))
            time.sleep(sleep_secs)
    return wrapper

try:
    for t in tasks:
        logger.info('Added worker: {}, run interval: {}s'.format(t[0].__name__, t[1]))
        threads.append(threading.Thread(target=auto_run(t[0], t[1]), name=t[0].__name__))
    for _t in threads:
        _t.start()
except Exception as ex:
    logger.exception(ex)
    sys.exit(1)

wsgi_app = make_wsgi_app()
