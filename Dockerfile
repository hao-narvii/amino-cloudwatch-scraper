FROM python:3.6
ENV PYTHONUNBUFFERED 1

WORKDIR /app
ADD ./requirements.txt /app
RUN pip install -r requirements.txt

ADD . /app
ENTRYPOINT [ "gunicorn", "server:wsgi_app", "--log-level", "DEBUG" ]
