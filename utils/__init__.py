import threading

from prometheus_client import Gauge


def synchronized(func):
    func.__lock__ = threading.Lock()

    def lock_func(*args, **kwargs):
        with func.__lock__:
            return func(*args, **kwargs)

    return lock_func


class SingletonGauge:
    instance = None
    current_gauge = None
    existed_gauges = {}
    @synchronized
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super().__new__(cls)
        return cls.instance
    def __init__(self, name, descr):
        if name not in self.existed_gauges:
            self.existed_gauges.update({name: Gauge(name, descr)})
        self.current_gauge = self.existed_gauges[name]
    def inc(self, val):
        self.current_gauge.inc(val)
